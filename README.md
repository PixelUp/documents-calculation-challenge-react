# Getting Started with React App

- Install NPM dependencies
- Run `npm start` or `yarn start`
- Open the browser to http://localhost:3000
- Make sure backend is working and on port 8080
- Pick sample `data.csv` for upload
- Add Currencies and Exchange rates like `EUR:1,USD:0.987,GBP:0.878`
- Pick Output currency and click `Generate Invoice`