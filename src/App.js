import './App.css';
import {useEffect, useState} from 'react';

function App() {

    const [file, setFile] = useState();
    const [currenciesInput, setCurrenciesInput] = useState(null);
    const [currencies, setCurrencies] = useState([]);
    const [output, setOutput] = useState();
    const [filter_VAT, setFilterVAT] = useState();
    const [result, setResult] = useState();
    const [error, setError] = useState();


    const submitInvoiceFile = async () => {

        setResult(null);
        setError(null);
        const formData = new FormData();
        formData.append('file', file);
        formData.append('input_currencies', currenciesInput);
        formData.append('output_currency', output);
        formData.append('filter_vat', filter_VAT);

        const res = await fetch('http://127.0.0.1:8000/api', {
            method: 'POST',
            body: formData
        }, {headers: {'Content-Type': 'application/json'}});
        const data = await res.json();
        if (data.success) {
            setResult(data.total);
        } else {
            setError(data.error);
        }
    }

    useEffect(() => {

        if (!currenciesInput) {
            return;
        }

        // split currencies by ,
        const currencies_array = currenciesInput.split(',');

        // split each currency array by : and take first element only
        const currencies_only = currencies_array.map(curr => curr.split(':')[0]);
        setCurrencies(currencies_only);
        console.log(currencies);

    }, [currenciesInput]);


    const handleSubmit = (event) => {
        event.preventDefault();
        try {
            submitInvoiceFile();
        } catch (e) {
            console.error(e);
            setResult(e);
        }
    }

    const handleFileChange = (event) => {
        setFile(event.target.files[0]);
    }

    return (
        <div>
            <form onSubmit={handleSubmit} className={'flex justify-center'}>
                <div className="py-12">
                    <h2 className="text-2xl font-bold">Invoicing challenge</h2>
                    <div className="mt-8 max-w-md">
                        <div className="grid grid-cols-1 gap-6">
                            <label className="block">
                                <span className="text-gray-700">Invoice file<small
                                    className={'text-red-500 text-lg'}>*</small></span>
                                <input type="file" name="file" onChange={handleFileChange} accept={'.csv'}
                                       className=" mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"/>
                            </label>
                            <label className="block">
                                <span className="text-gray-700">Currencies and Exchange rates<small
                                    className={'text-red-500 text-lg'}>*</small></span>
                                <input type="text"
                                       onChange={(e) => setCurrenciesInput(e.target.value)}
                                       placeholder={'EUR:1,USD:0.987,GBP:0.878'}
                                       className=" mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"/>
                            </label>
                            <label className="block">
                                <span className="text-gray-700">Output currency<small
                                    className={'text-red-500 text-lg'}>*</small></span>
                                <select onChange={(e) => setOutput(e.target.value)}
                                        className=" block w-full mt-1 rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 ">
                                    <option disabled selected>Pick currency</option>
                                    {currencies.map(currency => <option key={currency}
                                                                        value={currency}>{currency}</option>)}
                                </select>
                            </label>
                            <label className="block">
                                <span className="text-gray-700">
                                    Filter by VAT
                                    <small>(Optional)</small>
                                </span>
                                <input type="text"
                                       onChange={(e) => setFilterVAT(e.target.value)}
                                       placeholder={'1234567890'}
                                       className=" mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"/>
                            </label>
                            <button type={'submit'}
                                    className="mt-4 w-full bg-indigo-500 text-white font-bold py-2 rounded-md hover:bg-indigo-600 focus:outline-none focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                                Generate invoice
                            </button>
                        </div>
                    </div>
                    {result &&
                        <div>
                            Result: <pre className={'text-lg'}>{JSON.stringify(result, null, 2)}</pre>
                        </div>
                    }
                    {error &&
                        <div>
                            Error: <pre className={'text-lg text-red-500'}>{JSON.stringify(error, null, 2)}</pre>
                        </div>
                    }
                </div>
            </form>
        </div>
    );
}

export default App;
